//Manage autocompletion for artiste's blaze.
$(function(){

    var artistes = new Bloodhound({

        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: 'http://localhost:4000/searchArtistes'
    });

    $('#searchBlazeArtiste').typeahead(null, {
        name: 'artistes',
        source: artistes
    });
})