-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 20 Mars 2019 à 10:51
-- Version du serveur :  5.7.25-0ubuntu0.18.04.2
-- Version de PHP :  7.2.15-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `agentApp`
--

-- --------------------------------------------------------

--
-- Structure de la table `artistes`
--

CREATE TABLE `artistes` (
  `id` int(11) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `blaze` varchar(255) DEFAULT NULL,
  `story` text NOT NULL,
  `phone` int(11) NOT NULL,
  `mail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `artistes`
--

INSERT INTO `artistes` (`id`, `firstName`, `lastName`, `blaze`, `story`, `phone`, `mail`) VALUES
(1, 'Jean', 'Miche', 'Mich much', 'On s\'est rencontré l\'année dernière.', 74754757, 'jean@gmail.fr'),
(2, 'Sylvia', 'Dominique', 'Domi', 'Elle est gentille', 75858648, 'domi@gmail.fr'),
(3, 'Jean', 'Miche', 'Mich much', 'On s\'est rencontré l\'année dernière.', 74754757, 'jean@gmail.fr'),
(4, 'Sylvia', 'Dominique', 'Domi', 'Elle est gentille', 75858648, 'domi@gmail.fr'),
(5, 'fgfd', 'efr', 'rter', 'etze', 786, 'kdhf'),
(6, 'fgfdhgfhf', 'efr', 'rter', 'etze', 786, 'kdhf'),
(7, '', '', '', '', 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `edition`
--

CREATE TABLE `edition` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `story` text NOT NULL,
  `phone` int(11) NOT NULL,
  `mail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `edition`
--

INSERT INTO `edition` (`id`, `name`, `story`, `phone`, `mail`) VALUES
(1, 'Edition Lumière', 'Il sont super fort!', 67476, 'edit@gmail.fr'),
(2, 'Colorado', 'Collectif américain', 67475, 'col@gmail.fr'),
(3, 'Edition Lumière', 'Il sont super fort!', 67476, 'edit@gmail.fr'),
(4, 'Colorado', 'Collectif américain', 67475, 'col@gmail.fr');

-- --------------------------------------------------------

--
-- Structure de la table `fanzines`
--

CREATE TABLE `fanzines` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `edition` varchar(255) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `description` text,
  `format` varchar(3) DEFAULT NULL,
  `entrust` int(11) DEFAULT NULL,
  `sold` int(11) DEFAULT NULL,
  `inStock` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `fanzines`
--

INSERT INTO `fanzines` (`id`, `title`, `author`, `edition`, `year`, `description`, `format`, `entrust`, `sold`, `inStock`, `price`) VALUES
(1, 'Le lapin', 'Jean', 'Lumière', 2012, 'kutigiug', 'A3', 10, 2, 8, 10),
(2, 'soyeux', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50),
(3, 'lou', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000),
(4, 'Le rugeux', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20),
(5, 'Le rugeux', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20),
(6, 'le soin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60),
(7, 'ghgkj', '', '', 0000, '', '', 0, 0, 0, 90),
(8, 'lala', '', '', 0000, '', '', 0, 0, 0, 99),
(9, 'dsg', '', '', 0000, '', '', 0, 0, 0, 0),
(10, 'fanzine', '', '', 0000, '', '', 0, 0, 0, 89),
(11, 'lalatry', '', '', 0000, '', '', 0, 0, 0, 9),
(12, '     dfgfg', '', '', 0000, '', '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `activities` text,
  `phone` int(11) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `partners`
--

INSERT INTO `partners` (`id`, `name`, `description`, `activities`, `phone`, `mail`) VALUES
(1, 'Un test de parnenaire', 'Ils sont sympa il font bcp de chose', '[\"Sérigraphie\", \"pingpong\"]', 865657, 'dsfd@dsfsdf.fr');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `artistes`
--
ALTER TABLE `artistes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `edition`
--
ALTER TABLE `edition`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `fanzines`
--
ALTER TABLE `fanzines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author` (`author`),
  ADD KEY `edition` (`edition`);

--
-- Index pour la table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `artistes`
--
ALTER TABLE `artistes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `edition`
--
ALTER TABLE `edition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `fanzines`
--
ALTER TABLE `fanzines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
