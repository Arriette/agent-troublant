import React, { Component } from 'react';

class Partners extends Component {

    state = {
        partners: [],
        partner: {
            name: '',
            desription: '',
            activities: [],
            phone: 0,
            mail: '',
        }
    };

    componentDidMount() {
        this.getPartners();
    }

    getPartners = _ => {
        fetch('http://localhost:4000/partners')
            .then(response => response.json())
            .then(response => this.setState({partners: response.data }))
            .catch(err => console.error(err))
    }

    addPartners = _ => {
        const { partner } = this.state;
        fetch(`http://localhost:4000/partner/add?name=${partner.name}&description=${partner.description}&activities=${partner.activities}&phone=${partner.phone}&mail=${partner.mail}`)
            .catch(err => console.error(err))
    }

    renderPartner = ({id, name, desription, activities, phone, mail}) =>
        <div key={id}>{name}-{desription}-{activities}-{phone}-{mail}</div>

    render() {
        const { partners, partner } = this.state;
        return (
            <div className="Partners">
                {partners.map(this.renderPartner)}
                <div>
                    <input
                        value={partner.name}
                        onChange={e => this.setState({fanzine: {...partner, name: e.target.value}})}/>
                    <input
                        value={partner.description}
                        onChange={e => this.setState({fanzine: {...partner, description: e.target.value}})}/>
                    <input
                        value={partner.activities}
                        onChange={e => this.setState({fanzine: {...partner, activities: e.target.value}})}/>
                    <input
                        value={partner.phone}
                        onChange={e => this.setState({fanzine: {...partner, phone: e.target.value}})}/>
                    <input
                        value={partner.mail}
                        onChange={e => this.setState({fanzine: {...partner, mail: e.target.value}})}/>
                    <button
                        onClick={this.addPartners}
                    >Add</button>
                </div>

            </div>
        );
    }
}

export default Partners;
