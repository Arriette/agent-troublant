import React, { Component } from 'react';

class Artistes extends Component {

    state = {
        artistes: [],
        artiste: {
            firstName: '',
            lastName: '',
            blaze: '',
            story: '',
            phone: 0,
            mail: ''
        }
    };

    componentDidMount() {
        this.getArtistes();
    }

    getArtistes = _ => {
        fetch('http://localhost:4000/artistes')
            .then(response => response.json())
            .then(response => this.setState({artistes: response.data }))
            .catch(err => console.error(err))
    }

    addArtiste = _ => {
        const { artiste } = this.state;
        fetch(`http://localhost:4000/artiste/add?firstName=${artiste.firstName}&lastName=${artiste.lastName}&blaze=${artiste.blaze}&story=${artiste.story}&phone=${artiste.phone}&mail=${artiste.mail}`)
            .then(this.getArtistes)
            .catch(err => console.error(err))
    }

    renderArtiste = ({id, firstName, lastName, blaze, story, phone, mail}) =>
        <div key={id}>{firstName}-{lastName}-{blaze}-{story}-{phone}-{mail}</div>

    render() {
        const { artistes, artiste } = this.state;
        return (
            <div className="Fanzines">
                {artistes.map(this.renderArtiste)}
                <div>
                    <input
                        value={artiste.firstName}
                        onChange={e => this.setState({artiste: {...artiste, firstName: e.target.value}})}/>
                    <input
                        value={artiste.lastName}
                        onChange={e => this.setState({artiste: {...artiste, lastName: e.target.value}})}/>
                    <input
                        value={artiste.blaze}
                        onChange={e => this.setState({artiste: {...artiste, blaze: e.target.value}})}/>
                    <input
                        value={artiste.story}
                        onChange={e => this.setState({artiste: {...artiste, story: e.target.value}})}/>
                    <input
                        value={artiste.phone}
                        onChange={e => this.setState({artiste: {...artiste, phone: e.target.value}})}/>
                    <input
                        value={artiste.mail}
                        onChange={e => this.setState({artiste: {...artiste, mail: e.target.value}})}/>
                    <button
                        onClick={this.addArtiste}
                    >Add</button>
                </div>
            </div>
        );
    }
}

export default Artistes;