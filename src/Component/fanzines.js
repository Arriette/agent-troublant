import React, { Component } from 'react';

class Fanzines extends Component {

    state = {
        fanzines: [],
        fanzine: {
            title: '',
            author: '',
            edition: '',
            year: 0,
            description: '',
            format: '',
            entrust: 0,
            sold: 0,
            inStock: 0,
            price: 0
        }
    };

    componentDidMount() {
        this.getFanzines();
    }

    getFanzines = _ => {
        fetch('http://localhost:4000/fanzines')
            .then(response => response.json())
            .then(response => this.setState({fanzines: response.data }))
            .catch(err => console.error(err))
    }

    addFanzine = _ => {
        const { fanzine } = this.state;
        fetch(`http://localhost:4000/fanzine/add?title=${fanzine.title}&author=${fanzine.author}&edition=${fanzine.edition}&year=${fanzine.year}&description=${fanzine.description}&format=${fanzine.format}&entrust=${fanzine.entrust}&sold=${fanzine.sold}&inStock=${fanzine.inStock}&price=${fanzine.price}`)
            .then(this.getFanzines)
            .catch(err => console.error(err))
    }

    renderFanzine = ({id, title, author, edition, year, description, format, entrust, sold, inStock, price}) =>
        <div key={id}>{title}-{author}-{edition}-{year}-{description}-{format}-{entrust}-{sold}-{inStock}-{price}</div>

    render() {
        const { fanzines, fanzine } = this.state;
        return (
            <div className="Fanzines">
                {fanzines.map(this.renderFanzine)}
                <div>
                    <input
                        value={fanzine.title}
                        onChange={e => this.setState({fanzine: {...fanzine, title: e.target.value}})}/>
                    <input
                        value={fanzine.author}
                        onChange={e => this.setState({fanzine: {...fanzine, author: e.target.value}})}/>
                    <label htmlFor="typeaheadBlaze">author<input id="searchBlazeArtiste" type="text" name="typeaheadBlaze" className="typeahead" autoComplete="off" spellCheck="false"/>
                    </label>
                    <input
                        value={fanzine.edition}
                        onChange={e => this.setState({fanzine: {...fanzine, edition: e.target.value}})}/>
                    <input
                        value={fanzine.year}
                        onChange={e => this.setState({fanzine: {...fanzine, year: e.target.value}})}/>
                    <input
                        value={fanzine.description}
                        onChange={e => this.setState({fanzine: {...fanzine, description: e.target.value}})}/>
                    <input
                        value={fanzine.format}
                        onChange={e => this.setState({fanzine: {...fanzine, format: e.target.value}})}/>
                    <input
                        value={fanzine.entrust}
                        onChange={e => this.setState({fanzine: {...fanzine, entrust: e.target.value}})}/>
                    <input
                        value={fanzine.sold}
                        onChange={e => this.setState({fanzine: {...fanzine, sold: e.target.value}})}/>
                    <input
                        value={fanzine.inStock}
                        onChange={e => this.setState({fanzine: {...fanzine, inStock: e.target.value}})}/>
                    <input
                        value={fanzine.price}
                        onChange={e => this.setState({fanzine: {...fanzine, price: e.target.value}})}/>
                    <button
                        onClick={this.addFanzine}
                    >Add</button>
                </div>

            </div>
        );
    }
}

export default Fanzines;
