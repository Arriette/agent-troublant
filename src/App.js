import React, { Component } from 'react';
import './App.css';
import Fanzines from "./Component/fanzines";
import Artistes from "./Component/artistes";
import Partners from "./Component/partners";

class App extends Component {


  render() {
    return (
      <div className="App">
          <div>
              <h1>Fanzines</h1>
            <Fanzines/>
              <h1>Artistes</h1>
            <Artistes/>
              <h1>Partners</h1>
              <Partners/>
          </div>
      </div>
    );
  }
}

export default App;
