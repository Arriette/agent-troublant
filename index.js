const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const app = express();

//SQL REQUEST
const SELECT_ALL_FANZINES_QUERY = 'SELECT * FROM fanzines';
const SELECT_ALL_ARTISTES_QUERY = 'SELECT * FROM artistes';
const AUTOCOMPLETE_BLAZE_QUERY = `SELECT blaze FROM artistes`;
const SELECT_ALL_PARTNERS_QUERY = 'SELECT * FROM partners';


//DATABASE CONNECTION
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'agentApp'
});

connection.connect(err => {
    if(err) {
        return err;
    }
});

app.use(cors());



//ROADS

app.get('/', (req, res) => {
    res.send('hello page /')
});

//Autocomplete system for artistes blaze.
app.get('/searchArtistes', (req, res) => {

    connection.query(AUTOCOMPLETE_BLAZE_QUERY, (err, rows,) => {
        if (err){
            return res.send(err)
        }
        else {
            var data=[];
            for (var i = 0; i< rows.length; i++) {
                data.push(rows[i].blaze)
            }
            return res.send(JSON.stringify(data))
        }
    })
});


//Retrieve data fanzines.
app.get('/fanzine/add', (req, res) => {
    const {title, author, edition, year, description, format, entrust, sold, inStock, price} = req.query;
    const INSERT_FANZINES_QUERY = `INSERT INTO fanzines (title, author, edition, year, description, format, entrust, sold, inStock, price ) 
    VALUES('${title}', '${author}', '${edition}', ${year}, '${description}', '${format}', ${entrust}, ${sold}, ${inStock}, ${price}) `;

    connection.query(INSERT_FANZINES_QUERY, (err, result) => {
        if (err){
            return res.send(err)
        }
        else {
            return res.send('succefully added fanzines')
        }
    })
});

app.get('/fanzines', (req, res) => {

    connection.query(SELECT_ALL_FANZINES_QUERY, (err, results) => {
        if (err){
            return res.send(err)
        }
        else {
            return res.json( {
                data: results
            })
        }
    })
});


//Retrieve data artistes
app.get('/artiste/add', (req, res) => {
    const {firstName, lastName, blaze, story, phone, mail} = req.query;
    const INSERT_ARTISTES_QUERY = `INSERT INTO artistes (firstName, lastName, blaze, story, phone, mail ) 
    VALUES('${firstName}', '${lastName}', '${blaze}', '${story}', ${phone}, '${mail}' )`;

    connection.query(INSERT_ARTISTES_QUERY, (err, result) => {
        if (err){
            return res.send(err)
        }
        else {
            return res.send('succefully added artistes')
        }
    })
});

app.get('/artistes', (req, res) => {

    connection.query(SELECT_ALL_ARTISTES_QUERY, (err, results) => {
        if (err){
            return res.send(err)
        }
        else {
            return res.json( {
                data: results
            })
        }
    })
});


//Retrieve data partners
app.get('/partner/add', (req, res) => {
    const {name, description, activities, phone,mail} = req.query;
    const INSERT_PARTNERS_QUERY = `INSERT INTO partners (name, description, activities, phone, mail) 
    VALUES('${name}', '${description}', '${activities}', ${phone}, '${mail}',)`;

    connection.query(INSERT_PARTNERS_QUERY, (err, result) => {
        if (err){
            return res.send(err)
        }
        else {
            return res.send('succefully added parers')
        }
    })
});

app.get('/partners', (req, res) => {

    connection.query(SELECT_ALL_PARTNERS_QUERY, (err, results) => {
        if (err){
            return res.send(err)
        }
        else {
            return res.json( {
                data: results
            })
        }
    })
});

app.listen(4000, () => {
    console.log('Le server est démarré...')
    console.log('Ouvrez un autre terminal et tapez dans le même répertoir "yarn start"')
});