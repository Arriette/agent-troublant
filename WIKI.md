## Goal, wiki, todo entre autres...

Salut Manu !
Je suis trop contente que tu me file la patte sur ce projet !
Je l'ai recommencé plusieurs fois. J'étais mal organisée, c'était le chaos. Je me dis que cette fois c'est la bonne!
J'ai jamais fait de gestion de projet, c'est un excellent exo que de bosser à deux la dessus!


Ce wiki est un feotus encore difforme. Mais je souhaite le faire évoluer au fur et à mesure pour que n'importe quelle fanzinothèque puisse le prendre en main pour ses propres besoins.
Pour l'instant c'est à la fois nos spec, l'expression des besoins, notre wiki et notre redmine ^^

  ______               _   _                         _ _ _    __
 |  ____|             | | (_)                       | (_) |  /_/
 | |__ ___  _ __   ___| |_ _  ___  _ __  _ __   __ _| |_| |_ ___  ___
 |  __/ _ \| '_ \ / __| __| |/ _ \| '_ \| '_ \ / _` | | | __/ _ \/ __|
 | | | (_) | | | | (__| |_| | (_) | | | | | | | (_| | | | ||  __/\__ \
 |_|  \___/|_| |_|\___|\__|_|\___/|_| |_|_| |_|\__,_|_|_|\__\___||___/





Les fonctionnalités :
LEXIQUE :
Element géré : artiste, fanzine ou partenaires

MENU
- Un systeme de connexion d'utilisateurs.
- Des liens d'ajouts d'élement géré.
- consultation de stats

ACCEUIL
- Barre de recherche(*1) d'élement géré.
- Liste des plus consultés ou/et des recemment ajoutés qui devient suggestions quand on rentre qqchose dans la barre de recherche.
=> Au clic sur une suggestion on est renvoyé sur un page dédiée.

PAGE DEDIEE
- On a toutes les info relatives à un élement géré avec différentes options selon l'élément :

==artistes et parnetaire==
    -Modifié n'importe quelle info.
    -Toute modification doit mettre à jours les relations avec les fanzines.
    ex : si je change le blaze de l'artiste, tous les fanzine qu'il a crée doivent être mis à jour sur l'author.

==Fanzine== => toutes les infos et des action possible :
    -Gérer une vente :
        1. On rentre le nombre de vendu et on valide.
        2. Demande de confirmation qui récap tout pour etre sur.
        3. Dans la base de donnée, "instock" est décrémenté, "sold" est incrémenté en euros.

    - Un artiste vient récupérer sa tune :
        1. On clique sur une touche faite pour ca;
        2. Intrust et sold sont remis à zéro.
        3. Un message nous indique ce qu'il faut donner à l'artiste (sold*0,70)


LES STATS EN PLUS :
    - Ce que l'on doit aux artistes (totalSold*0,7)  <== Le stats le plus important pour gérer nos finances.
    -A réfléchir ..



*1 => Barre de recherche avec autocomplétion au moyen de la librairie js typeahead avec Bloodhound(les deux sont dans le fichier typeahead.bundle.js), c'est ce qu'utilise twitter pour ses recherches. J'ai fait un test dans un input "author" dans le formulaire d'ajout de fanzine). Affichage des 10 premiers éléments.
Je ne suis pas fixé sur les critère de filtre mais il faudrait pouvoir tous chercher (artiste, fanzine et partenaire dans une seule barre.

ADAPTATION MOBILE :
Perso, je pars de zéro. Je me dis qu'on verra ça à la fin mais je ne suis pas sur que ce soit la meilleurs des idées..




   _____                   _             __ _    __    __      _ _
  / ____|                 (_)           /_/| |  /_/   / _|    (_) |
 | |     ___    __ _ _   _ _    __ _    ___| |_ ___  | |_ __ _ _| |_
 | |    / _ \  / _` | | | | |  / _` |  / _ \ __/ _ \ |  _/ _` | | __|
 | |___|  __/ | (_| | |_| | | | (_| | |  __/ ||  __/ | || (_| | | |_
  \_____\___|  \__, |\__,_|_|  \__,_|  \___|\__\___| |_| \__,_|_|\__|
                  | |
                  |_|
L'architecture de la base de donnée est quasiement faite mais sujette à des ajout de champs et des champs qui seront pas la suite requis (il ne le sont pas mtn pour faire des tests plus rapidement.
Le dump est à la racine du projet et s'appelle AgentDB.

Toutes les données sont récupérés mais les liens entre les tables ne sont pas tous fait.




   _____                                     _                       _                             _            ___
  / ____|                                   | |                     ( )                           (_)          |__ \
 | |     ___  _ __ ___  _ __ ___   ___ _ __ | |_    ___  _ __    ___|/  ___  _ __ __ _  __ _ _ __  _ ___  ___     ) |
 | |    / _ \| '_ ` _ \| '_ ` _ \ / _ \ '_ \| __|  / _ \| '_ \  / __|  / _ \| '__/ _` |/ _` | '_ \| / __|/ _ \   / /
 | |___| (_) | | | | | | | | | | |  __/ | | | |_  | (_) | | | | \__ \ | (_) | | | (_| | (_| | | | | \__ \  __/  |_|
  \_____\___/|_| |_| |_|_| |_| |_|\___|_| |_|\__|  \___/|_| |_| |___/  \___/|_|  \__, |\__,_|_| |_|_|___/\___|  (_)
                                                                                  __/ |
                                                                                 |___/
20/03/19
Alice : Je suis en train de plancher sur les barres de recherche.



           _ _            _            __      _             _ _                            _                           _   _
     /\   | (_)          | |          / _|    (_)           ( |_)                          | |                         (_) | |
    /  \  | |_  ___ ___  | |_ _   _  | |_ __ _ _ ___   _ __ |/ _ _ __ ___  _ __   ___  _ __| |_ ___    __ _ _   _  ___  _  | |
   / /\ \ | | |/ __/ _ \ | __| | | | |  _/ _` | / __| | '_ \  | | '_ ` _ \| '_ \ / _ \| '__| __/ _ \  / _` | | | |/ _ \| | | |
  / ____ \| | | (_|  __/ | |_| |_| | | || (_| | \__ \ | | | | | | | | | | | |_) | (_) | |  | ||  __/ | (_| | |_| | (_) | | |_|
 /_/    \_\_|_|\___\___|  \__|\__,_| |_| \__,_|_|___/ |_| |_| |_|_| |_| |_| .__/ \___/|_|   \__\___|  \__, |\__,_|\___/|_| (_)
                                                                          | |                            | |
                                                                          |_|                            |_|

Ici, n'hésite pas à me faire des remarques sur mon code, mes façons de faire ou des moyens de rendre l'appli plus cool.


